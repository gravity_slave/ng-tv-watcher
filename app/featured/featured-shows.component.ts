import {Component, OnInit} from '@angular/core';
import {Featured} from "./featured";
import {FeaturedService} from "./featured.service";

@Component({
  moduleId: module.id,
  selector: 'tv-featured-shows',
  templateUrl: 'featured-shows.template.html',
  styleUrls: ['styles.css']
})

export class FeaturedShowsComponent implements OnInit{
  shows: Featured[];
  errMsg: string;

  constructor(private featuredService:FeaturedService) {

  }
  ngOnInit() {
    this.getFeatures();
  }

  private getFeatures(): void {
    this.featuredService.featured()
        .subscribe((features: Array<Featured>) => this.shows = features)

  }
}