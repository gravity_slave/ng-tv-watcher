import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Featured } from "./featured";

@Injectable()
export class FeaturedService {
  private featureURL: string = 'api/featured_shows.json';

  constructor(private _http: Http) { }

  featured(): Observable<Featured[]> {
    return this._http.get(this.featureURL)
        .map((res: Response) => res.json() as Array<Featured>)
        .do(data => console.log(data))
        .catch(this.handleError)
  }

  private handleError(err: Response) {
    let msg = `Error status code ${err.status} status ${err.statusText} ar ${err.url}`;
    return Observable.throw(msg);
  }
}