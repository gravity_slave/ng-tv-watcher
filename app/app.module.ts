import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';
import { CarouselModule } from "./carousel/carousel.module";
import { UIModule } from './ui/ui.module';
import { HttpModule} from "@angular/http";
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

import './shared/rxjs-extensions';
import {FeaturedModule} from "./featured/featured.module";
import {SearchModule} from "./search/search.module";

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        UIModule,
        CarouselModule,
        FeaturedModule,
        SearchModule,
        AppRoutingModule

    ],
    declarations: [ AppComponent,
        HomeComponent
    ],
    providers: [ ],
    bootstrap:    [ AppComponent ]
})

export class AppModule { }