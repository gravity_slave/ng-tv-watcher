import { Component, OnInit } from '@angular/core';
import { CarouselService } from './carousel.service';
import { Carousel } from './carousel';

@Component({
  moduleId: module.id,
  selector: 'tv-carousel-container',
  templateUrl: 'carousel-container.template.html'
})

export class CarouselContainerComponent implements OnInit {
  slides: Carousel[];
  errorMsg: string;

  constructor(private  _carouseService: CarouselService) {}

  ngOnInit() {
    this.getSlides();
  }

  private getSlides() {
    this._carouseService.featured()
        .subscribe(
            slides => this.slides = slides,
        err => this.errorMsg = err as any );
  }

}