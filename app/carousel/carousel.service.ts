import { Injectable } from '@angular/core';
import { Http,  Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Carousel } from './carousel';

@Injectable()
export class CarouselService {
  private carouselUrl = 'api/carousel_items.json';
  constructor(private _http: Http) { }

  featured(): Observable<Array<Carousel>> {
    return this._http.get(this.carouselUrl)
        .map( (response: Response) => response.json() as Array<Carousel>)
        .do(data => console.log(data))
        .catch(this.handleError);
  }

  private handleError(err: Response) {
    let msg = `Erroro status code ${err.status} status ${err.statusText} at ${err.url}`;
    return Observable.throw(msg);

  }
}